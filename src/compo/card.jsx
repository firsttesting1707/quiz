import './card.scss'

const Card = (props) => {
    return(
        <>
            <div className="card">
                <img src={props.im} alt="" />
                <p>{props.name}</p>
                <div className="bcon">
                    <a href={props.pdf} target="_blank">Tải PDF</a>
                    <a href={props.word} target="_blank">Tải Word</a>
                </div>
            </div>
        </>
    )
}

export default Card