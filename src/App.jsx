import { useState } from 'react'
import reactLogo from './assets/react.svg'
import viteLogo from '/vite.svg'
import './App.css'
import Card from './compo/card'

function App() {

  return (
    <section className='home'>
      <Card im='/geo.png' name="102 câu TN Địa lý (55 đề chọn lọc)" pdf="/file/geo_102.docx" word="/file/geo_102.docx"></Card>
      <Card im='/lit.png' name="Văn mẫu Tây Tiến" pdf='/file/taytien1.pdf' word='taytien1.docx'></Card>
      <Card im='/his.png' name="80 câu TN Lịch Sử" pdf='/file/his_80.pdf' word='/file/his_80.docx'></Card>
      <Card im='/lit.png' name="Văn mẫu Sóng" pdf='/file/song.pdf' word='/file/song.docx'></Card>
      <Card im='/lit.png' name="Văn mẫu Đất nước" pdf='/file/dn.pdf' word='/file/dn.docx'></Card>
      <Card im='/lit.png' name="Văn mẫu Việt Bắc" pdf='/file/vb.pdf' word='/file/vb.docx'></Card>
      <Card im='/lit.png' name="Văn mẫu Vợ chồng A Phủ" pdf='/file/vcap.pdf' word='/file/vcap.docx'></Card>
      <Card im='/lit.png' name="Tóm tắt luận điểm 3 bài thơ" pdf='/file/tt.pdf' word='/file/tt.docx'></Card>
    </section>
  )
}

export default App
